const getRandomInt = (max: number): number => Math.floor(Math.random() * Math.floor(max));

const range = (n: number): number[] => ([...Array(n).keys()]);

const transposing = (arr: number[][]): number[][] => arr[0].map((_: any, colIndex: number) => arr.map(row => row[colIndex]));

const swapRowsSmall = (arr: number[][], n: number): number[][] => {
    const copyArr = [...arr.map(el => [...el])];
    const area = getRandomInt(n);
    const line1 = getRandomInt(n);

    const n1 = area*n + line1;

    let line2 = getRandomInt(n);
    while(line1 === line2) {
        line2 = getRandomInt(n);
    }

    const n2 = area*n + line2;

    [copyArr[n1], copyArr[n2]] = [copyArr[n2], copyArr[n1]];

    return copyArr;
};

const swapColumnsSmall = (arr: number[][], n: number): number[][] => transposing(swapRowsSmall(transposing(arr), n));

const swapRowsArea = (arr: number[][], n: number): number[][] => {
    const copyArr = [...arr.map(el => [...el])];
    const area1 = getRandomInt(n);

    let area2 = getRandomInt(n);

    while(area1 === area2) {
        area2 = getRandomInt(n);
    }

    range(n).forEach(i => {
        const n1 = area1*n + i;
        const n2 = area2*n + i;
        [copyArr[n1], copyArr[n2]] = [copyArr[n2], copyArr[n1]];
    });

    return copyArr;
};

const swapColumnsArea = (arr: number[][], n: number): number[][] => transposing(swapRowsArea(transposing(arr), n));

const fillIn = (n: number): number[][] => range(n*n).map(i => range(n*n).map(j => Math.floor((i*n + i/n + j) % (n*n) + 1)));

const mix = (arr: number[][], n: number, amt: number = 10): number[][] => {
    if (amt === 0) {
        return arr;
    }

    const mixFunctions = [
        transposing,
        swapRowsSmall,
        swapColumnsSmall,
        swapColumnsArea
    ];

    return mix(mixFunctions[getRandomInt(mixFunctions.length)](arr, n), n, --amt);
};

const nonPossibleNumbers = (arr: number[][], i: number, j: number): any => {
    const nonPossibilities: any = {};

    for(let k = 0; k < arr.length; k++) {
        if(arr[i][k] > 0) {
            nonPossibilities[arr[i][k]] = true;
        }
        if(arr[k][j] > 0) {
            nonPossibilities[arr[k][j]] = true;
        }
    }

    for(let iBox = Math.floor(i / 3) * 3; iBox < Math.floor(i / 3) * 3 + 3; iBox++) {
        for(let jBox = Math.floor(j / 3) * 3; jBox < Math.floor(j / 3) * 3 + 3; jBox++) {
            if(arr[iBox][jBox]) {
                nonPossibilities[arr[iBox][jBox]] = true;
            }
        }
    }

    return nonPossibilities;
};

const isResolveable = (arr: number[][]): boolean => {
    for(let i = 0; i < arr.length; i++) {
        for(let j = 0; j < arr.length; j++) {
            if(arr[i][j] === 0) {
                const nonPossibilities = nonPossibleNumbers(arr, i, j);
                const impossibleNumbers = Object.keys(nonPossibilities);

                if(impossibleNumbers.length === arr.length - 1) {
                    return true;
                }
            }
        }
    }

    return false;
};

export const generatePuzzle = (arr: number[][]): number[][] => {
    const copyArr = [...arr.map(el => [...el])];
    const difficult = Math.pow(copyArr.length, 4);

    let iterator = 0;
    while(iterator < difficult) {
        const i = getRandomInt(copyArr.length);
        const j = getRandomInt(copyArr.length);

        if(copyArr[i][j] !== 0) {
            iterator++;

            const temp = copyArr[i][j];
            copyArr[i][j] = 0;

            if (isResolveable(copyArr)) {
                continue;
            }

            copyArr[i][j] = temp;
        }
    }

    return copyArr;
};

export const generateNumbers = (n: number = 3): number[][] => mix(fillIn(n), n);
