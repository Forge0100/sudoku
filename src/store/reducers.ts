export interface State {
  grid: number[][],
  solvedGrid: number[][],
  active: {
    row: number,
    cell: number,
  },
  username: string,
  time: number,
  isGameStarted: boolean
}

export interface Action {
  type: string,
  payload?: any
}

export const initialState: State = {
  grid: [],
  solvedGrid: [],
  active: {
    row: -1,
    cell: -1
  },
  username: 'Anonymous',
  time: 0,
  isGameStarted: false
};

export const reducer = (state: State, action: Action): State => {
  switch(action.type) {
    case 'setGrid':
      return { ...state, grid: action.payload };
    case 'setSolvedGrid':
      return { ...state, solvedGrid: action.payload };
    case 'setCellValue': {
      const { row, cell, value } = action.payload;

      return {
        ...state,
        grid: state.grid.map((stateRow, rowIndex) => {
          if (rowIndex === row) {
            return stateRow.map((stateCell, cellIndex) => {
              if (cellIndex === cell) {
                return value;
              }

              return stateCell;
            });
          }

          return stateRow;
        })
      }
    }
    case 'setActiveCell':
      return {
        ...state,
        active: {
          row: action.payload.row,
          cell: action.payload.cell
        }
      };
    case 'setDefaultActiveCell':
      return { ...state, active: initialState.active };
    case 'setUsername':
      return { ...state, username: action.payload };
    case 'setTimeSeconds':
      return { ...state, time: action.payload };
    case 'resetTimeSeconds':
      return { ...state, time: initialState.time };
    case 'setStartGame':
      return { ...state, isGameStarted: action.payload };
    default:
      return state;
  }
};
