import React, {Dispatch, memo, useCallback, useEffect, useReducer} from 'react';
import classNames from 'classnames';
import {reducer, initialState, State, Action} from './store';
import * as Sudoku from './utils/sudoku';
import './App.css';

function App() {
  const [state, dispatch]: [State, Dispatch<Action>] = useReducer(reducer, initialState);
  const { grid, solvedGrid, active, username, time, isGameStarted } = state;
  const buttons = [...Array(9).keys()];

  const generatePuzzle = useCallback(() => {
    const solvedGrid = Sudoku.generateNumbers();
    const grid = Sudoku.generatePuzzle(solvedGrid).map(row => row.map(cell => cell !== 0 ? cell : null));

    dispatch({ type: 'setSolvedGrid', payload: solvedGrid });
    dispatch({ type: 'setGrid', payload: grid });
    dispatch({ type: 'resetTimeSeconds' });
    dispatch({ type: 'setStartGame', payload: false });
  }, [dispatch]);

  const isCellInvalid = useCallback((row, col, value) => {
    if (!value) {
      return true
    }

    for (let c = 0; c < grid.length; c += 1) {
      if (grid[row][c] === value && c !== col) {
        return true
      }
    }

    for (let r = 0; r < grid.length; r += 1) {
      if (grid[r][col] === value && r !== row) {
        return true
      }
    }

    const rowStart = Math.floor(row / 3) * 3;
    const colStart = Math.floor(col / 3) * 3;
    for (let r = rowStart; r < rowStart + 3; r++) {
      for (let c = colStart; c < colStart + 3; c++) {
        if (grid[r][c] === value && !(r === row && c === col)) {
          return true
        }
      }
    }

    return false
  }, [grid]);

  const isGameComplete = useCallback(() => {
    for (let r = 0; r < 9; r++) {
      for (let c = 0; c < 9; c++) {
        if (isCellInvalid(r, c, grid[r][c])) {
          return false;
        }
      }
    }

    return true;
  }, [grid, isCellInvalid]);

  const setCellValue = useCallback((value) => () => {
    const { row, cell } = active;

    dispatch({ type: 'setCellValue', payload: { row, cell, value } });
    dispatch({ type: 'setDefaultActiveCell' });

    if (isGameComplete()) {
      alert(`Congratulations, ${username}. Time: ${time}`);
      generatePuzzle();
    }
  }, [active, isGameComplete, generatePuzzle, username, time]);

  const handleUsername = useCallback((event) => {
    dispatch({ type: 'setUsername', payload: event.target.value })
  }, [dispatch]);

  const handleShowAnswer = useCallback(() => {
    dispatch({ type: 'setDefaultActiveCell' });
    dispatch({ type: 'setGrid', payload: solvedGrid })
  }, [dispatch, solvedGrid]);

  const cellClassNames = useCallback(({ rowIndex, cellIndex }: { rowIndex: number, cellIndex: number }) => {
    return classNames(
      'cell',
      {
        'border-right': cellIndex === 2 || cellIndex === 5,
        'border-bottom': rowIndex === 2 || rowIndex === 5,
        'active': active.row === rowIndex && active.cell === cellIndex,
        'original': grid[rowIndex][cellIndex],
        'invalid': grid[rowIndex][cellIndex] && isCellInvalid(rowIndex, cellIndex, grid[rowIndex][cellIndex]),
      });
  }, [active, grid, isCellInvalid]);

  const setActiveCell = useCallback((row: number, cell: number) => () => {
    if(grid[row][cell]) {
      return;
    }

    if(!isGameStarted) {
      dispatch({ type: 'setStartGame', payload: true });
    }

    dispatch({ type: 'setActiveCell', payload: { row, cell } })
  }, [grid, isGameStarted]);

  useEffect(() => {
    if (!isGameStarted) {
      return;
    }

    const timer: NodeJS.Timeout = setInterval(() => {
      dispatch({ type: 'setTimeSeconds', payload: time + 1 })
    }, 1000);

    return () => clearInterval(timer);
  }, [time, isGameStarted]);

  useEffect(() => {
    generatePuzzle();
  }, [generatePuzzle]);

  return (
    <div className="App">
      <div className="row">
        <h2 className="header">Sudoku</h2>
        <input type="text" placeholder="Username" onChange={handleUsername} />
      </div>
      <div className="row">
        <button onClick={handleShowAnswer}>Show answer</button>
        <button onClick={generatePuzzle}>Restart</button>
        <span>Time: {time}</span>
      </div>

      <div className="grid">
        {grid.map((row, rowIndex) => (
          <div key={rowIndex} className="row">
            {row.map((cell, cellIndex) => (
              <div
                key={cellIndex}
                className={cellClassNames({ rowIndex, cellIndex })}
                onClick={setActiveCell(rowIndex, cellIndex)}
              >{cell}</div>
            ))}
          </div>
        ))}
      </div>

      <div className="row">
        {buttons.map((value: number) => (
          <button
            key={value}
            className="btn"
            disabled={active.row === -1 || active.cell === -1}
            onClick={setCellValue(value + 1)}
          >
            {value + 1}
          </button>
        ))}
      </div>
    </div>
  );
}

export default memo(App);
