Game is written on ReactJS with using TypeScript syntax and Hooks approach.

## Sudoku algorithm
1. Fill the 9x9 grid with numbers from 1 to 9 and shift the numbering in each next area by 1
2. Transpose and change columns, rows and grid areas in random order
3. Remove random cells from the grid in turn, checking the ability to solve the grid. If there can be several numbers in the cells, you need to return the cell back and find the next one to delete (**the more cells removed the harder to solve the grid**)

## How to run the application (User)
1. Open the sudoku game page
2. Enter the username in the field
3. Click on any field in the grid without a number and select the desired number using the number buttons below (**timer starts after pressing the numeric button**)
4. This and another cell with the same number will be highlighted in red if you enter the wrong number in the cell
5. To display the result, you can click on "Show answer" button
6. To reset the game, you can click on "Restart" button

## How to run the application (Developer)
1. Install packages using `npm install`
2. Run application using `npm start`
3. Open the sudoku game page in a browser